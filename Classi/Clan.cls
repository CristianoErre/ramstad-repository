public class Clan {
    
    /* Attributi */
    
    //La gilda
    public  SingleGilda             gilda               {get; set;}
    //Nome del Clan
    public  String                  nome                {get; set;}
    //Deposito del clan
    public  Banca                   deposito            {get; set;}
    //Amministrazione
    public  Amministrazione         funzionari          {get; set;}
    //Consiglio
    public  Consiglio               counsil             {get; set;}
    //Partner di commercio internazionale
    public  InternationalTrade      myPartner           {get; set;}
    //Lista di beni posseduti dal Clan
    public  Map<String, Decimal>    goodsList           {get; set;} 
    
    public final Map<String, Decimal> M1 = new Map<String, Decimal>();
    

    /* Costruttori */
    
    public Clan () {
        //Inizializzo la gilda in singola istanza
        gilda = SingleGilda.makeGildaInstance();
        counsil = new Consiglio('Svalbar');
        deposito = new Banca('Dei nani', 3 , 1555555.3);
    }
    
    /* Metodi */
    
    /// Metodi di gestione del Clan
    

    //#33 inizializza Amministrazione
    public void initAmministrazione(String nome)
    {
        Amministrazione admin = new Amministrazione(nome);
    }
    //#32
    //TODO inizializza una lista di membri (prende come argomento una lista di memebri)
     List<SingleGilda.Membro> listaFunzionari = new List<SingleGilda.Membro>() ;
    //#31
    //TODO elimina un membro
    public void eliminaMembro(String nome){
            integer i = getIndexByName(nome);
            listaFunzionari.remove(i);
        }

         public Integer getIndexByName(String nome){
            integer i = 0;
                for(SingleGilda.Membro m : listaFunzionari){
                    if(m.nome.equals(nome)){
                        return i;
                    }
                    i++;
                } return null;
        }
    
    //#30
    //TODO cerca un membro
    public void AddMember(SingleGilda.Membro ma){
    
                listaFunzionari.add(ma);
        }
    
    //#29
    //TODO aggiunge bottino
    public void AddBottino(decimal soldi){
        
        Gilda.AggiungeSoldi(soldi);
        
    }
    
    //#28
    //TODO spende bottino
    public void spendiBottino(decimal spesa){
        
        Gilda.PrelievoSoldi(spesa);
        
    }
    
    //#27
    //TODO metodo che conta il numero di membri per ogni classe e segnala se è una proporzione stabile:
            //Guerriero                 5
            //Ladro                     6
            //Bardo                     2
            //Mago                      2
            //Barbaro Mangia Bambini    1 {proporzione ideale}
            /*La funzione restituisce un codice a colori a secoda della distanza dalla proporzione ideale*/
    public String proporzionieStabile()
    {
        Map<String, Integer> vMappa = gilda.countMemberClass1();
        Integer res1 = 0;
        Integer res2 = 0;
        Integer res3 = 0;
        Integer res4 = 0;
        Integer res5 = 0;
        for(String key : vMappa.keySet())
            {
                if(key == 'Guerriero')
                {
                    
                    res1 = math.mod(vMappa.get(key),5);
                }
                if(key == 'Ladro')
                {
                    
                    res2 = math.mod(vMappa.get(key),6);
                }
                if(key == 'Bardo')
                {
                    
                    res3 = math.mod(vMappa.get(key),2);
                }
                if(key == 'Mago')
                {
                   
                    res4 = math.mod(vMappa.get(key),1);
                }
                if(key == 'Barbaro Mangia Bambini')
                {
                    
                    res5 = math.mod(vMappa.get(key),1);
                }
            }
            Decimal media = (res1 + res2 + res3 + res4 + res5)/5;
            String risultato = '';
            if(media == 0)
            {
                risultato = 'verde';
            }
            else if(media > 0 && media < 1)
            {
                risultato = 'giallo';
            }
            else
            {
                risultato = 'rosso';
            }
            return risultato;
    }
    
    //#26
    //TODO metodo che cambia classe ad un membro (il clan spende soldi a seconda di che licenza deve acquistare)
    public void CambiaClasse (String Na, String Cla) {
         Decimal res = M1.get(cla);
         if(deposito.SottraeEuro(res)){
         gilda.cambioClasse(Na,cla);
         }
     }
    //#25
    //TODO metodo che deposita n euro in banca solo se il tasso d'interesse è maggiore del 3%
        //I soldi sono tolti dal bottino e aggiunti al conto
    public void depositaSoldi(Decimal soldi){
        if(deposito.tassoInteresse >= 3){
            deposito.addEuroConto(soldi);
            gilda.PrelievoSoldi(soldi);
        } else{ throw new ClanException('Canbia banca che ti sta fregando', '007');}
    }
    //#24
    //TODO preleva dal deposito in banca (impossibile se non ci sono abbastanza soldi)
        //I soldi sono tolti dal conto e aggiuinti al bottino
    public void prelevaSoldi(Decimal moneta){
        deposito.SottraeEuro(moneta);
        gilda.AggiungeSoldi(moneta);
        
    }
    //#23
    //TODO metodo che converte il bottino in yen, rupie, dollari o sterline
    public decimal getAmmontare2(String myCurrency)
    {
        return gilda.getAmmontare1(myCurrency);
    }
    
     //#22
    //TODO metodo che "promuove" un membro nell'Amministrazione (lo elimina dalla lista membri e lo aggiunge all'Amministrazione)
    public void amministrationPromotion(SingleGilda.membro goodPlayer){
        gilda.eliminaMembro(goodPlayer);
        funzionari.AddMember(goodPlayer);
    }
    
    //#21
    //TODO metodo che "promuove" un membro dell'Amministrazione nel consiglio (lo elimina dalla lista..)
    public void councilPromotion(SingleGilda.membro topPlayer){
        funzionari.eliminaMembroAmministrazione(topPlayer.nome);
        counsil.addConsiglieri(topPlayer);
    }
    
    
    /// Metodi per il commercio
    
    //#20
    //TODO metodo che inizializza l'InternationalTrade
    public void initInternationalTrade(String nome, Banca conto)
    {
        InternationalTrade intTrade = new InternationalTrade (nome, conto);
    }
    //#19
    //TODO metodo che acquista n° beni da un partner internazionale (deve tenere conto del cambio) e li aggiunge alla lista del Clan
     public void acquista(InternationalTrade comp,List<String> beni){
        Decimal tot = 0;
        for(String tre : beni){
            for(String bue : comp.goodslist.keySet()){
                if(tre.equals(bue)){
                    goodsList.put(bue,comp.goodslist.get(bue));
                    tot += comp.goodslist.get(bue);
                    comp.vendi(bue);
                }
            }
        }
        
        tot = gilda.getAmmontare1(comp.myCurrency);
        prelevaSoldi(tot);
    }
    
    
    /// Metodi di gestione delle Missioni
    
    
    //#18
    //TODO Un membro di classe guerriero esegue una missione nel dungeon e accumula 50*(la difficoltà del livello) di esperienza
            //ATTENZIONE: un guerriero può completare il dungeon solo se il suo livello è maggiore del livello di
            //difficoltà del dungeon
            public void EssegueMissione (Dungeon du ,String nom){
            
                integer re = getIndexByName2(nome);
                SingleGilda.Membro mem= listaFunzionari.get(re);
               if((re != null)&& (du.difficulty<= mem.livello ))
                    mem.addEsperienza(50*du.difficulty);
                }

     public Integer getIndexByName2(String nome){
            integer i = 0;
                for(SingleGilda.Membro m : listaFunzionari){
                    if(m.nome.equals(nome)){
                        return i;
                    }
                    i++;
                    }
                    return null;
                }
    //#17
    //TODO metodo che, se ci sono almeno 10.000 euro per ogni membro aggiunge 1000 d'esperienza ma toglie 1000Xn° membri €
    public void EuroForExp()
    {
        Boolean check = true;
        for(SingleGilda.Membro m : listaFunzionari)
        {
            if (gilda.bottinoDisponibileEuro() < 10000) {
                check= false;
            }
        }
        Decimal fee = 1000*listaFunzionari.size();
        if(check =  true)
        {
            gilda.aggiungeExp(1000);
            gilda.PrelievoSoldi(fee);
        }
    }
    //#16
    //TODO metodo che converte 1000 euro in 100 punti esperienza
    public void convertPoint()
    {
        gilda.ConverteToEsperienza();
    }
    
    //
    
    
    /************************************************************************************************/
    
    /* Classi */
    
    //Consiglio del clan
    public class Consiglio {
        private List<SingleGilda.Membro>    consiglieri     {get; set;}
        public  String                      nomeConsiglio   {get; set;}         
        
        public Consiglio (String nomeConsiglio){
            this.nomeConsiglio  = nomeConsiglio;
            this.consiglieri = new List<SingleGilda.Membro>{};
        }
        
        //#15
        //TODO Metodo che aggiunge un elemento alla lista dei consiglieri
        public void addConsiglieri(SingleGilda.Membro nuovoMembro) {
            consiglieri.add(nuovoMembro);
        }
		
		
        //#14
        //TODO metodo che elimina un membro dalla lista dei consiglieri
        public void RemoveConsiglioByName(String nome){
            
            integer index = getIndexConsiglioByName1(nome);
            consiglieri.remove(index);
            
        }
        
        public Integer getIndexConsiglioByName1(String nome){
            integer i = 0;
                for(SingleGilda.Membro m : consiglieri){
                    if(m.nome.equals(nome)){
                        return i;
                    }
                    i++;
                } return null;
        }
        
        
    }
    
    //I funzionari del clan
    public class Amministrazione {
        
        public final List<String>           POSSIBLESENIORITY   = new List<String> { 'Junior', 'Senior', 'Major' };
        
        private List<SingleGilda.Membro>    funzionari              {get; set;}
        public  String                      nomeAmministrazione     {get; set;}
        public  String                      livelloDiSeniority      {get; set;}
        
        public Amministrazione (String nomeAmministrazione){
            this.nomeAmministrazione    = nomeAmministrazione;
        }
        
        //#13
        //TODO metodo che aggiunge un membro dell'amministrazione
        public void AddMember(SingleGilda.Membro ma){
            
            if(funzionari == null){
                
                funzionari = new List<SingleGilda.Membro>{};
                funzionari.add(ma);
            }else{
                
                funzionari.add(ma);
                
            }
        }
        
        //#12
        //TODO metodo che elimina un membro dell'amministrazione
        public void eliminaMembroAmministrazione(String nome){
            integer i = getIndexByName(nome);
            funzionari.remove(i);
        }

         public Integer getIndexByName(String nome){
            integer i = 0;
                for(SingleGilda.Membro m : funzionari){
                    if(m.nome.equals(nome)){
                        return i;
                    }
                    i++;
                } return null;
        }
        
        //#11
        //TODO metodo che setta la seniority scegliendo tra quelle possibili
        public void setSeniority(String sen){
            for(String s : POSSIBLESENIORITY){
                if(s.equals(sen)){
                    this.livelloDiSeniority = sen;
                }
            }
        }
    }
    
    //Dungeon per le missioni
    public class Dungeon {
        
        public String   name            {get; set;}
        public Integer  difficulty      {get; set;}
        
        public Dungeon () {}
        
        public Dungeon (String name, Integer difficulty){
            this.name       = name;
            this.difficulty = difficulty;
        }
        
    }
    
    //Banca per depositare il bottino
    public class Banca {
        
        public final Decimal TASSO_DI_INTERESSE  = 4.4;
        
        public  String       nome                    {get; set;}
        public  Decimal       tassoInteresse          {get; set;}
        //Conto monetario espresso in euro
        private Decimal      conto                   {get; set;}
        //Soldi in prestito
        private Decimal      soldiPrestati           {get; set;}
        
        
        public Banca (String nome, Decimal tassoInteresse, Decimal conto){
            this.nome               = nome;
            this.tassoInteresse     = tassoInteresse;
            this.conto              = conto;
            
            this.soldiPrestati      = 0.0;
        }
        
        //#10
        //TODO metodo che ritorna l'ammontare del conto
        public decimal getAmmontareConto(){
            return conto;
        }
        
        //#09
        //TODO metodo che aggiunge euro al conto
        public void addEuroConto(decimal addedEuro){
            conto += addedEuro;
        }
        
        //#08
        //TODO metodo che sottrae euro al conto (il conto non può andare in rosso)
        public Boolean SottraeEuro(decimal sold){
            if(conto >= sold ){
            conto= conto - sold;
            return true; 
            } else {
                throw new ClanException('Invalid Prelievo', '001');
                return false;
            }
        }
        //#07
        //TODO metodo che chiede un prestito alla banca ad un dato tasso d'interesse
        public void askLoan(Decimal loan)
        {
            soldiPrestati += loan + (loan * tassoInteresse /100);
        }
        
    }
    
    public class InternationalTrade {
        
        public Set<String> POSSIBLECURRENCY = new Set<String> { 'euro', 'dollari', 'sterline', 'yen', 'rupie' };
        
        private String                      myCurrency      {get; set;}
        public  String                      companyName     {get; set;}
        public  Map<String, Decimal>        goodsList       {get; set;}
        private Banca                       conto           {get; set;}
        
        //Lista dei movimenti
        public  List<MovimentoBancario>     movimenti   {get; set;}
        
        public InternationalTrade (String companyName, Banca conto) {
            this.companyName    = companyName;
            this.conto          = conto;
            this.goodsList = new Map<String, Decimal>();
        }
        
        //#06
        //TODO metodo che imposta la myCurrency (deve essere tra quelle possibili)
         public void setCurrency(String valuta){
            if(POSSIBLECURRENCY.contains(valuta)){
                this.myCurrency = valuta;
            } else {throw new ClanException('Invalid currency', '001');
            }
        }
        //#05
        //TODO metodo che aggiunge un elemento alla goodsList <Nome, prezzo>
		public void addElementogoodsList(Map<String, Decimal> goodsList, String Nome, Decimal Prezzo) {
        		    goodslist.put(Nome, Prezzo);
        }
        
        //#04
        //TODO metodo che aggiunge un elemento alla goodsList e sottrae il prezzo al conto (non può andare in rosso)
        public void AgguigeElemento (String a , Decimal b) {
            
            goodsList.put(a,b);
            conto.SottraeEuro(b);
            
            
        }
        
        
        //#03
        //TODO metodo vendi che cerca un elemento nella goodsList e lo elimina aggiungendo il prezzo al conto (richiama l'aggiunta del movimento bancario)
        public void vendi(String element){
            Map<String, Decimal> curr = new map<String, Decimal>();
            String id_mov = '';
            for(String key : goodsList.keySet()){
            if(key.equals(element)){
                id_mov = '' + goodsList.size() + '';
                goodsList.remove(key);
                conto.addEuroConto(goodsList.get(key));
                curr.put(key,goodsList.get(key));
            }
            } MovimentoBancario mov = new MovimentoBancario(id_mov,curr);
            AddMovimentoBancario(mov);
            
           
        }
        //#02
        //TODO metodo che aggiunge un MovimentoBancario
        public void AddMovimentoBancario(MovimentoBancario mb){
            
            if(movimenti == null){
                
                movimenti = new List<MovimentoBancario>{};
                movimenti.add(mb);
                
            }else{
            
            	movimenti.add(mb);}
            
        }
        
    }
    
    public class MovimentoBancario {
        
        public  String                  idMovimentoBancario         {get; set;}
        public  Map<String, Decimal>    elencoBeniAcquistati        {get; set;}
        public  Decimal                 totaleMonetario             {get; set;}
        
        public MovimentoBancario (String idMovimentoBancario, Map<String, Decimal> elencoBeniAcquistati){
            this.idMovimentoBancario    = idMovimentoBancario;
            this.elencoBeniAcquistati   = elencoBeniAcquistati;
            this.totaleMonetario        = calculateTotaleMonetario();
        }
        
        //#01
        //TODO metodo che calcola automaticamente il totale monetario dato un elenco di beni
        public Decimal calculateTotaleMonetario (){ 
            Decimal totale = 0;
            for(String key : elencoBeniAcquistati.keySet())
            {
                totale += elencoBeniAcquistati.get(key);
            } 
            return totale; 
        }
        
    }

 public class ClanException extends exception {
        
        public String message;
        public String code;
        
        public ClanException (String message, String code){
            this.message    = message;
            this.code       = code;
        }
        
    }

}







