/*La classe SingleGilda è gestita in Singleton; in ogni dato
  momento ne può esistere una unica istanza*/
public class SingleGilda {
    
     public  static final String    GUER        = 'Guerriero'            ;
     public  static final String    MAGO        = 'Mago'                 ;
     public  static final String    LADRO       = 'Ladro'                ;
     public  static final String    BARDO       = 'Bardo'                ;
     public  static final String    BARBARO     = 'Barbaro'              ;
     public  static final String    CACCIATORI = 'Cacciatori di test'   ;
     
     public static final Set<String>  SetClass = new  Set<String>{GUER, MAGO, LADRO, BARDO, BARBARO, CACCIATORI};

    
     private static Gilda   myGuild     {get; set;}
    
    //Il costruttore è privato in modo che non possano essere create n istanze
    private SingleGilda () {}
    
    /*  Istanzia la gilda solo se non è già inizializzata
     *  i due seguenti costruttori seguono logiche differenti
     *  (solitamente in una classe Singleton ne è presente solo uno)
     */
    public static void makeGilda (){
        if ( myGuild == null ){
            myGuild = new Gilda();
        }
    }
    public static SingleGilda makeGildaInstance (){
        if ( myGuild == null ){
            return new SingleGilda();
        }
        return null;
    }
    
    public Map<String, Integer> countMemberClass1(){
        return myGuild.countMemberClass();
    }
    //#32 get ammontare

    public decimal getAmmontare1(String myCurrency)
    {
        return myGuild.bottino.getAmmontare(myCurrency);
    }
    //#31
    //TODO metodo che crea un nuovo membro
    public void aggiungiMembro(Membro nuovo){
        myGuild.nuovoMembro(nuovo);
    }
    //#30
    //TODO metodo che elimina un membro
    public void eliminaMembro(Membro nabbo){
        myGuild.removeMembro(nabbo.nome);
    }
    //#29
    //TODO metodo che aggiunge esperienza ad un membro
     public void aggiungeExp(decimal exp)
        {
            myGuild.addExp(exp);
        }
    //#28
    //TODO metodo che cambia la classe di un membro
    public void cambioClasse(String Name,String cla){
       myGuild.ChangeClass(Name,cla);
    }
    
    //#27
    //TODO metodo che informa della quantità di tesoro presente
    
     public Decimal bottinoDisponibileEuro(){
        return myGuild.convertiBottino('euro');
    }
    //#26
    //TODO metodo che aggiunge soldi al tesoro
     public void AggiungeSoldi (Decimal m){
        myGuild.addValuebottino(m);
    }
    //#25
    //TODO metodo che toglie soldi al tesoro
    public void PrelievoSoldi (Decimal m){
        myGuild.addBooty(m);
    }
    //#24
    //TODO metodo che informa sulla classe più popolare
   public String classePiuPopolare(){
         Integer max = 0;
         String classe = '';
        Map<String,Integer> mappa = myGuild.countMemberClass();
        for(String key : mappa.keySet()){
            if(mappa.get(key) > max){
                max = mappa.get(key);
                classe = key;
            }
            }return classe;
        } 

    //#23
    //TODO metodo che restituisce il/i membro/i più forte/i
    public List<Membro> piuForte(){
        return myGuild.personaggioLivelloMassimo();
    }
    //#22
    //TODO metodo che converte 1000 euro in 100 punti esperienza
    public void ConverteToEsperienza (){ 
        myGuild.addBooty(1000);
        myGuild.addExp(100);
    }
    
    //#21
    //TODO metodo che, se ci sono almeno 10.000 euro per ogni membro aggiunge 1000 d'esperienza ma toglie 1000Xn° membri €
    public void strangeTransaction()
    {
        Boolean check = true;
        for(Membro m : myGuild.membri)
        {
            if (bottinoDisponibileEuro() < 10000) {
                check= false;
            }
        }
        Decimal fee = 1000*myGuild.membri.size();
        if(check =  true)
        {
            aggiungeExp(1000);
            PrelievoSoldi(fee);
        }
    } 

    public class Gilda {
        
        //Elenco dei membri della gilda
        private List<Membro>     membri      {get; set;}
        
        //Il bottino raccolto dalla gilda
        public Tesoro           bottino     {get; set;}
        
        //L'esperienza accumolata dalla gilda
        public Decimal          esperienza  {get; set;}
        
        //Il livello della gilda, calcolato automaticamente
        public Integer          livello     {get; set;}
        
        //Costruttore
        public Gilda (){
            membri      = new List<Membro>();
            bottino     = new Tesoro();
            esperienza  = 0.0;
            livello     = 0;
        }
        
        public void setGuildLvl(List<Membro> membriGilda)
        {
            decimal expCounter = 0;
            integer livello = 0;
            for(Membro m: membriGilda)
            {
               expCounter += m.esperienza;
            }
            livello = (expCounter / 1000).intValue();
        }
        
        //#19
        //TODO metodo per creare un nuovo membro della gilda (richiama il ricalcolo dell'esperienza)
        public void nuovoMembro (Membro nuovo) {
            membri.add(nuovo);
            setGuildLvl(membri);
        }
        
        //#18
        //TODO metodo per eliminare un membro (richiama il ricalcolo dell'esperienza)
        public void removeMembro (string nome)
        {
            Integer i = getIndexByName(nome);
            membri.remove(i);
            setGuildLvl(membri);
        }
        
        //#17
        //TODO metodo che cambia la classe di un membro
        public void ChangeClass (String Name,String clas ){
            Membro m = cercaMembroPerNome(Name);
            
            try {
                m.setClasse(clas);
            }catch (GuildException ex){
                  System.debug('ERROR:' + ex);
            }catch(exception e) {
            }
        }
        
        //#16
        //TODO metodo che restituisce il numero di membri
        
        public  integer numerdiMembri(){
            return(membri.size());
        }
        
        //#15
        //TODO metodo che ristituisce la lista di membri
        public List<Membro> returnMemberList(){
            List<Membro> listaMembri = new List<Membro>();
            for(Membro fabio : membri){
                listaMembri.add(fabio);
            }
                return listaMembri;
            }
        
        //#14
        //TODO metodo per cercare un membro (dal nome)
        public Membro cercaMembroPerNome(String nome){
                for(Membro m : membri){
                    if(m.nome.equals(nome)){
                        return m;
                    }
                } return null;
        }


        //#14 v2
        //Restituisce indice del membro cercato nella lista membri
        public Integer getIndexByName(String nome){
            integer i = 0;
                for(Membro m : membri){
                    if(m.nome.equals(nome)){
                        return i;
                    }
                    i++;
                } return null;
        }
        //#13
        //TODO metodo che restituisce il/i membro/i con il livello più alto

         public List<Membro> personaggioLivelloMassimo(){
            Integer max = 0;
            List<Membro> topPlayer;
                for(Membro m : membri){
                    if(m.livello > max){
                        max = m.livello;
                        topPlayer = new List<Membro>();
                        topPlayer.add(m);
                    }
                    if(m.livello == max){
                        topPlayer.add(m);
                    }
                }
                return topPlayer;
        }
        
        //#12
        //TODO metodo che aggiunge esperienza ad un membro (richiama il ricalcolo dell'esperienza)
        public void addExp(decimal exp)
        {
            esperienza += exp;
            setGuildLvl(membri);

        }
        
        //#11
        //TODO metodo per aggiungere bottino 
        public  void addValuebottino(decimal addedValue){
            bottino.addValueTesoro(addedValue) ;
        }
        
        //#10
        //TODO metodo per togliere bottino
        public  void addBooty(decimal addedBooty){
            bottino.subtractValueTesoro(addedBooty);
        }
        
        //#09
        //TODO metodo che calcola quanto bottino c'è per ogni membro
         public Decimal bottinoPersonale(String curen){
            return (bottino.getAmmontare(curen)/ membri.size());
        }
        //#08
        //TODO metodo per contare quanti membri ha la gilda per ogni classe

        public Map<String, Integer> countMemberClass()
        {
            integer cGuer=0;
            integer cMago=0; 
            integer cLadro =0;
            integer cBardo =0;
            integer cBarbaro =0;
            integer cCacciatore = 0;
            Map<String, Integer> m1 = new Map<String, Integer>();

            
            for(Membro m: membri)
            {
                if(m.classe == 'Guerriero')
                {
                    cGuer ++;
                }
                else if(m.classe == 'Mago')
                {
                    cMago ++;
                }
                else if(m.classe == 'Ladro')
                {
                    cLadro ++;
                }
                else if(m.classe == 'Bardo')
                {
                    cBardo ++;
                }
                else if(m.classe == 'Barbaro')
                {
                    cBarbaro ++;
                }
                else if(m.classe == 'Cacciatori di test')
                {
                     cCacciatore ++;
                }
            }
            m1.put('Guerriero', cGuer);
            m1.put('Mago', cMago);
            m1.put('Ladro', cLadro);
            m1.put('Bardo', cBardo);
            m1.put('Barbaro', cBarbaro);
            m1.put('Cacciatori di test', cCacciatore);
            
            return m1;
            
        }

         public Decimal convertiBottino(String valuta){
            return bottino.getAmmontare(valuta);
        }
    }
    
    public class Membro {
        
        private String  classe          {get; set;}
        
        public  String  nome            {get; set;}
        public  Integer eta             {get; set;}
        
        private Decimal esperienza      {get; set;}
        public  Integer livello         {get; set;}
        
        public Membro (String nome, Integer eta, Decimal esperienza){
            this.nome       = nome;
            this.eta        = eta;
            this.esperienza = esperienza;
        }
        
        //#07
        //TODO metodo per impostare la classe, il numero possibile di classi deve essere limitato
        public void setClasse (String classe){  
            if(SetClass.Contains(classe))
            {
               this.classe=classe;
            }else{
               throw new GuildException('Invalid myValue', '001');
            }

        }
        
        //#06
        //TODO modifica il livello a seconda dell'esperienza (ogni 100 punti esperienza un livello)
        public void setLivello (Decimal esperienza){
            Integer levelup = (esperienza / 100).intValue();
            livello = levelup;
        }
        
        //#05
        //TODO metodo che aggiunge esperienza tenendo conto del cambio di livello
        public void addEsperienza (Decimal nuovaEsperienza){
            Decimal expTot = nuovaEsperienza + esperienza;
            setLivello(expTot);
        }
        
        //#04
		//TODO metodo che dà le informazioni sulla possibilità o meno del membro di bere Martini
        public Boolean isInDrinkingAge (){ 
            if(eta < 18)
            {
                return false;
            }
            return true; }
		}	
    
    public class Tesoro {
        
        //Ammontare del tesoro in euro
        private        double myCurrency         {get; set;}
        private        double euro               {get; set;}

        private  final double dollari            {get; set;}
        private  final double sterline           {get; set;}
        private  final double yen                {get; set;}
        private  final double rupie              {get; set;}
        private  Decimal      ammontare          {get; set;}
        
        public Tesoro () {
            this.euro       = 1       ;
            this.dollari    = 0.847   ;
            this.sterline   = 1.6370  ;
            this.yen        = 78.0000 ;
            this.rupie      = 70.2500 ;
        }
        
        public Tesoro (Decimal ammontare){
            this.ammontare = ammontare;
        }
        
        //#03
        //TODO metodo che aggiunge valore al tesoro
        public  void addValueTesoro(decimal addedValue){
            ammontare += addedValue;
        }
        
        //#02
        //TODO metodo che toglie valore al tesoro
        public  void subtractValueTesoro(decimal subtractedValue ){
            ammontare = ammontare - subtractedValue;
        }

        
        //#01
        //TODO metodo che ritorna l'ammontare in euro, yen, dollari, sterline o rupie
    

    public Decimal getAmmontare (String MyCrrency){ 
         
            if(MyCrrency.equalsIgnoreCase('euro')){
               
                return ammontare;}
            if(MyCrrency.equalsIgnoreCase('dollari')){
              
                return ammontare * dollari;            }
            if(MyCrrency.equalsIgnoreCase('sterline')){
                
                return ammontare * sterline;}  
            
            if(MyCrrency.equalsIgnoreCase('yen')){
                
                return ammontare * yen;}  
            if (MyCrrency.equalsIgnoreCase('rupie')){
                return ammontare * rupie;}
            return 0.0;
        }
    
}
    //Eccezione customizzata (usare il throw con questa eccezione se necessario)
    public class GuildException extends exception {
        
        public String message;
        public String code;
        
        public GuildException (String message, String code){
            this.message    = message;
            this.code       = code;
        }
        
    }

}