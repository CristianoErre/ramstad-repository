public class CittaStato {
    
    /*Attributi*/
    
    //Ogni clan occupa un quartiere cittadino
    public List<Clan>                       quartieriCittadini      {get; set;}
    //i soldi pubblici della città stato
    public SingleGilda.Tesoro               fondoCittadino          {get; set;}
    //La banca centrale della città stato
    public BancaCittadina                   bancaCentrale           {get; set;}
    //Il reggente della città stato
    public Reggente                         principe                {get; set;}
    //I partner commerciali della città
    public List<Clan.InternationalTrade>    myPartners              {get; set;}
    //Parlamento
    public ConsiglioCittadino               parlamento              {get; set;}
    //Senato
    public Consulta                         senato                  {get; set;}
    //I beni della città
    public Map<String, Decimal>             beniCittadini           {get; set;}
    //Potenziali acquirenti esterni
    public List<Acquirente>                 acquirenti              {get; set;}
    
    /*Costruttori*/
    //#28
    //TODO completare i costruttori
    public CittaStato (){
        this.quartieriCittadini = new List<Clan>();
        this.fondoCittadino     = new SingleGilda.Tesoro();
        this.myPartners         = new List<Clan.InternationalTrade>() ;
        this.parlamento         = new ConsiglioCittadino () ; 
        this.senato             = new Consulta() ; 
        this.beniCittadini      = new Map<String, Decimal>(); 
        this.acquirenti         = new List<Acquirente>() ;
    }
    public CittaStato (List<Clan> quartieriCittadini,SingleGilda.Tesoro fondoCittadino,BancaCittadina bancaCentrale,
                        Reggente principe,List<Clan.InternationalTrade> myPartners, ConsiglioCittadino parlamento,
                        Consulta senato, Map<String, Decimal> beniCittadini, List<Acquirente> acquirenti ){
        this.quartieriCittadini = quartieriCittadini;
        this.fondoCittadino=fondoCittadino;
        this.bancaCentrale = bancaCentrale;
        this.principe = principe; 
        this.myPartners = myPartners ;
        this.parlamento = parlamento; 
        this.senato= senato ; 
        this.beniCittadini = beniCittadini; 
        this.acquirenti = acquirenti ;
    }
    
    /*Metodi*/
    
    //#27
    //TODO metodo che aggiunge un cittadino alla lista e lo divide nel suo clan
    
    //#26
    //TODO cerca un membro per nome, restituisce anche il clan di appartenenza
    
    //#25
    //TODO elimina un membro, risarcisce il clan corrispondente per 1000*LIVELLO_MEMBRO
    
    //#24
    //TODO metodo di tassazione che prende il 10% dal tesoro di ogni clan e lo aggiunge al fondo cittadino
    
    //#29
    //TODO metodo che deposita tot soldi dal fondo cittadino alla banca centrale
    
    //#30
    //TODO metodo che preleva un po' di soldi dalla banca e li mette nel fondo cittadino
    
    //#31
    //TODO metodo che chiede un prestito alla banca centrale
    
    //#23
    //TODO metodo che controlla se in ogni quartiere il bilanciamento delle occupazioni è stabile
    
    //#22
    //TODO metodo che prende il consigliere(o parlamentare o senatore) di livello più alto e lo fa Principe; in caso di parità si fa una random
    
    //#21
    //TODO metodo che conta i funzionari totali della città
    
    //#20
    //TODO metodo che conta i consiglieri(di quartiere) totali della città
    
    //#19
    //TODO metodo che inizializza i myPartners prendendo i partner dei singoli quartieri
    
    //#18
    //TODO metodo che cerca un partner in myPartners
    
    //#43
    //TODO metodo che ordina i partner da quell che ha più beni a quello che ne ha meno
    
    //#17
    //TODO metodo che compra n beni da un partner (scala soldi dal tesoro cittadino)
    
    //#16
    //TODO metodo che permette ad un quartiere di comprare dal suo partner (scala soldi dal suo bottino)
    
    //#42
    //TODO metodo che vende n beni ad un acquirente e mette i soldi nel fondo cittadino

    //#44
    //TODO metodo che inizializza gli acquirenti prendendo i dati da un custom setting
            //VA FATTO UN CUSTOM SETTINGS
    
    //#36
    //TODO metodo che estrapola tutti i movimentiBancari da tutti i partner
        
    //#15
    //TODO metodo che aggiunge un clan/quartiere alla lista dei quartieri cittadini; per poter essere aggiunto
        //deve avere un minimo di 100 membri(contando anche consiglieri e funzionari)
        
    //#14
    //TODO metodo che conta l'esercito cittadino dividendo in plotoni(10 uomini) centurie(100) reggimenti(1000) legioni(6000)
                //solo i guerrieri rientrano nell'esercito
    
    //#13
    //TODO metodo che conta l'esercito di riseva dividendo in plotoni(10 uomini) centurie(100) reggimenti(1000) legioni(6000)
                //solo i barbari rientrano nell'esercito di riserva
                
    //#34
    //TODO metodo che calcola il costo mesile (con gli stipendi) dell'esercito cittadino
    
    //#35
    //TODO metodo che calcola il costo mensile (con gli stipendi) dell'esercito di riserva
    
    //#12
    //TODO metodo che prende i 100 tra bardi, ladri e barbari con il livello più alto e li mette in parlamento
    
    //#11
    //TODO metodo che prende i 100 maghi con il livello più alto e li mette in senato
    
    //#33
    //TODO metodo che calcola la differenza tra lo stipendio di un mago e quello di un senatore
    
    //#37
    //TODO metodo che calcola la differenza tra lo stipendio medio di bardi, ladri e barbari e quello di un parlamentare
    
    //#10
    //TODO metodo che conta la percentuale di ladri nella popolazione e restituisce un codice-colore indicatore di rischio
    
    //#38
    //TODO metodo che calcola la proporzione tra funzionari, consiglieri, senatori e parlamentari ed il resto della popolazione
    
    //#39
    //TODO metodo che calcola in percentuale quante persone sono nell'esercito sul totale
    
    //#40
    //TODO metodo che calcola il PIL (quanto fondo cittadino c'è per ogni cittadino)
    
    //#09
    //TODO metodo che restituisce la quantità di beni totali, sia quelli comuni cittadini che quelli dei quartieri
    
    //#08
    //TODO metodo che restituisce il prezzo medio tra tutti i beni della città, sia quelli comuni cittadini che quelli dei quartieri
    
    //#07   IMPORTANTE
    //TODO metodo che data una classe tra guerrieri, maghi, ladri, bardi, barbari e cacciatori di teste,
            // funzionari, consiglieri, parlamentari e senatori ne restituisce lo stipendio
            // VANNO CREATI DEI CUSTOM SETTINGS
            
    //#06
    //TODO metodo che restituisce quante mensilità in media un lavoratore delle classi base (non parlamentare o senatore)
            //deve lavorare per comprare il prezzo medio di tutti i beni
    
    //#05
    //TODO metodo che regola l'inflazione: se servono troppi mesi di lavoro per comprare il prezzo medio dei beni
            //brucia un po' di soldi dalla banca cittadina, se ne servono troppo pochi stampa del denaro
            
            
    //#32
    //TODO metodo che dato lo stipendio di una classe calcola quanti stipendi si possono pagare con 1/10 del fondo cittadino
            
    //#45
    //TODO metodo che prende la proporzione di funzionari/consiglieri di quartiere e parlamentari/senatori e controlla 
            //a quale proporzione in dei custom settings si avvicina di più
            //VANNO FATTI DEI CUSTOM SETTINGS
    
    public class Reggente{
        
        public  SingleGilda.Membro  datiReggente    {get; set;}
        private String              titolo          {get; set;}
        public List<String>			elenco_titoli	{get; set;}
        
        public Reggente (SingleGilda.Membro datiReggente){
            this.datiReggente   =   datiReggente;
        }
        
        //#04
        //TODO metodo che restituisce il titolo del reggente
        public string RestituisceTitolo(){
            return this.titolo;
        }
        
        //#03
        //TODO metodo che inizializza il titolo scegliendo tra una lista (duca, conte, marchese, visconte)
        public void InizializzaTitolo(String nuovoTitolo) {
            List<String> elenco_titoli = new List<String>();
        	elenco_titoli.add('duca');
            elenco_titoli.add('conte');
            elenco_titoli.add('marchese');
            elenco_titoli.add('visconte');
            try {
            	for(String abba : elenco_titoli) {
                	if (abba.equals(nuovoTitolo)) {
                    	this.titolo = nuovoTitolo ;
                	}
                 }
            } catch (Exception e) {
                            throw new CityException('Titolo non valido\nInserire un titolo tra duca, conte, marchese, visconte');                        }    
            }
        }
        
    }
    
    public class ConsiglioCittadino {
        
        public List<SingleGilda.Membro>     consiglieriCittadini    {get; set;}
        
        public ConsiglioCittadino (){
            this.consiglieriCittadini = new List<SingleGilda.Membro>();
        }
        
        public ConsiglioCittadino (List<SingleGilda.Membro> consiglieriCittadini){
            this.consiglieriCittadini = consiglieriCittadini;
        }
    }
    
    public class Consulta {
        
        public List<SingleGilda.Membro>     maghi           {get; set;}
        
        public Consulta (){
            this.maghi  = new List<SingleGilda.Membro>();
        }
        public Consulta (List<SingleGilda.Membro> maghi){
            this.maghi  = maghi;
        }
        
    }
    
    public class BancaCittadina {
        
        public  Clan.Banca       depositoBancaCittadina      {get; set;}
        
        public BancaCittadina (String nome, Double tassoInteresse, Decimal conto){
            depositoBancaCittadina = new Clan.Banca ( nome, tassoInteresse, conto);
        }
        
        //#02
        //TODO metodo che elimina una quantità di soldi
        public void eliminaSoldi(decimal money)
        {
            depositoBancaCittadina.SottraeEuro(money);
        }
        //#01
        //TODO metodo che stampa (aggiunge) una quantità di soldi
        public void aggiungiSoldi(decimal addSoldi){
            depositoBancaCittadina.addEuroConto(addSoldi);
            System.debug('i suoi soldi: '+addSoldi+'sono stati aggiunti');
        }
    }
    
    public class Acquirente {
        
        public String                   nome        {get; set;}
        public SingleGilda.Tesoro       budget      {get; set;}
        public Map<String, Decimal>     beni        {get; set;}
        
        public Acquirente (SingleGilda.Tesoro budget, String nome){
            beni        = new Map<String, Decimal>();
            this.budget = budget;
            this.nome   = nome;
        }
        
        //#41
        //TODO metodo per acquistare un bene (budget permettendo)
        
    }
    
    
    //#00
    //TODO Classe Exception
   public class CityException extends exception {
        
        public String message;
        public String code;
        
        public CityException (String message, String code){
            this.message    = message;
            this.code       = code;
        }
        
    }

}





