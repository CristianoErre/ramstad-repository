public virtual class TheIceMachine {
    
    public  List<Ice>   iceList         {get; set;}
    private Iceberg     iceberg         {get; set;}
    
    public TheIceMachine(){
        iceList = new List<Ice>();
    }
    
    public TheIceMachine(List<Ice> iceList){
        try {
            this.iceList = iceList.isEmpty() ? new List<Ice>() : iceList;
        }
        catch(Exception e){
            throw new IceException('No ice!', '0001');
        }
    }
    
    public static Ice getIce (String colour, Decimal volume, Decimal density, Integer hardness){
        
        return new Ice (colour, volume, density, hardness);
        
    }
    
    public String colourInspector (Ice cube){
        
        if (cube == null){
            return 'no ice';
        }
        
        if (cube.colour == null){
            return 'no colour';
        }
        
        if (cube.colour.equalsIgnoreCase('blue') || cube.colour.equalsIgnoreCase('blu')){
            return 'cool ice';
        }
         
        return 'nice ice';
        
    }
    
    public Ice breakTheIce (Ice cube){
        if ( (cube == null) || (cube.volume == null) || (cube.hardness > 5) ){
            return null;
        }
        cube.volume = cube.volume / 2;
        return cube;
    }
    
    public void MakeIceberg (){
        if (!isThereEnoughIce())
            throw new IceException('Not enaugh Ice', '0002');
            
        if (!isTheIceHardEnough())
            throw new IceException('The Ice is too soft', '0003');
            
        
        iceberg = new Iceberg(iceList);
    }
    
    private Boolean isTheIceHardEnough (){
        
        Integer total           = 0;
        Integer cubeNumber      = 0;
        
        for (Ice cube : iceList){
            try{
                total += cube.hardness;
                cubeNumber ++;
            }
            catch (Exception e){
                continue;
            }
        }
        
        return (total/cubeNumber > 10);
        
    }

    private Boolean isThereEnoughIce (){
        return (iceList.size() > 100);
    }

    public class Ice {
        
        public  String      colour          {get; set;}
        public  Decimal     volume          {get; set;} 
        public  Decimal     density         {get; set;} 
        public  Integer     hardness        {get; set;}
        
        public Ice (String colour, Decimal volume, Decimal density, Integer hardness){
            this.colour     = colour;
            this.volume     = volume;
            this.density    = density;
            this.hardness   = hardness;
        }
        
    }
    
    public class IceException extends exception {
        
        public String message;
        public String code;
        
        public IceException (String message, String code){
            this.message    = message;
            this.code       = code;
        }
        
    }
    
    private class Iceberg {
        
        private List<Ice>   icebergMass     {get; set;}
        
        public Iceberg (List<Ice> appropriateIce){
            this.icebergMass = appropriateIce;
        }
        
    }

}