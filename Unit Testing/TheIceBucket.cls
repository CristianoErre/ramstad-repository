public class TheIceBucket extends TheIceMachine {
    
    public      List<Ice>       bucket              {get; set;}
    public      Temperature     roomTemperature     {get; set;}
    
    private     waterGlass      water               {get; set;}
    private     Martini         martiniGlass        {get; set;}
    
    private      String         quality             {get; set;}
    private      Integer        alcoolPercentage    {get; set;}
    
    public TheIceBucket (){
        super(new List<Ice>());
        this.roomTemperature = new Temperature();
    }
    
    public void fillBucket(){
        bucket = iceList;
    }
    
    public void emptyBucket(){
        bucket = new List<Ice>();
    }
    
    public void destroyBucket(){
        bucket = null;
    }
    
    public void takeTheIceFromTheIceberg (){
        
        try {
            MakeIceberg();
        }catch (Exception e){
            throw new IceException('There is no iceberg', '1372');
        }
            
        bucket = new List<Ice>();
        
        for (Integer i = 0 ; i < 100; i ++){
            
            bucket.add(getIce('blue', 7.3, 8.6, 15));
            
        }
        
    }
    
    public void makeWater(Integer molecularAmount){
        
        water = new waterGlass(molecularAmount);
        
    }
    
    public boolean isTheWaterBalanced (){
        if (water == null)
            return false;
            
        if ( !(water.o.size() == (water.h.size()/2) ) )
            return false;
            
        return true;
    }
    
    public void noIceEmergency (){
        if ( bucket.isEmpty() )
            chemicallyMakeTheIce ();
    }
    
    private void chemicallyMakeTheIce (){
        
        if ( water == null )
            throw new IceException('No water', '4444');
            
        if ( !isTheWaterBalanced() )
            throw new IceException('Water not balanced', '5677');
        
        if ( !roomTemperature.isEnergyLowEnoughForIce() )
            throw new IceException('Too hot', '6666');
            
        bucket = chemicalIce();
        
    }
    
    private List<Ice> chemicalIce (){
        List<Ice> newIce = new List<Ice>();
        for (Integer i = 0; i<100; i++){
            newIce.add(getIce('purple', 15.3, 18.6, 7));
        }
        return newIce;
    }
    
    public void martiniWithIce(){
        if (!isANiceDayForAMartini() )
            throw new IceException('Technically is always a nice day for a Martini', '????');
            
        if ( (bucket == null)||(bucket.isEmpty()) )
            throw new IceException('No ice', '7767');
            
        martiniGlass = getMeAMartini();
    }
    
    private Boolean isANiceDayForAMartini (){
        return roomTemperature.niceDay();
    }
    
    public void setMeAMartini (String quality, Integer alcoolPercentage){
        this.quality            = quality;
        this.alcoolPercentage   = alcoolPercentage;
    }
    
    private Martini getMeAMartini (){
        if ( (quality!=null) && (alcoolPercentage!=null) )
            return new Martini(quality, alcoolPercentage);
        
        return null;
    }
    
    /*CLASS*/
    
    
    public class waterGlass {
        
        public List<Oxygen>     o       {get; set;}
        public List<Hydrogen>   h       {get; set;}
        
        public waterGlass(Integer o, Integer h){
            for (Integer j = 0; j<o; j++){
                this.o.add(new Oxygen());
            }
            for (Integer k = 0; k<h; k++){
                this.h.add(new Hydrogen());
            }
        }
        
        public waterGlass(Integer amount){
            
            o = new List<Oxygen>();
            h = new List<Hydrogen>();
            
            for ( Integer i=0; i<amount; i++ ){
                o.add(new Oxygen());
                h.add(new Hydrogen());
                h.add(new Hydrogen());
            }
        }

        
    }
    
    private class Oxygen {
        public Oxygen(){
        }
    }
    private class Hydrogen {
        public Hydrogen(){
        }
    }
    
    public class Temperature {
        
        private Integer energy       {get; set;}
        
        public Temperature (){
            energy = 100;
        }
        
        public void lowerTheEnergy (){
            energy -= 1;
        }
        
        public void moreEnergy (){
            energy += 1;
        }
        
        public Integer howManyDegrees (){
            return energy;
        }
        
        public Boolean niceDay (){
            return ( (energy > 24) && (energy < 34) );
        }
        
        public Boolean isEnergyLowEnoughForIce (){
            return (energy == 0);
        }
        
    }
    
    private class Martini {
        public  String  quality             {get; set;}
        public  Integer alcoolPercentage    {get; set;}
        
        public Martini (String quality, Integer alcoolPercentage){
            this.quality            =   quality;
            this.alcoolPercentage   =   alcoolPercentage;
        }
    }

}





